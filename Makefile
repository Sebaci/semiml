BINDIR = bin
TARGET = semiML
SOURCES = $(shell find src -regex ".*\\.ml\\([ily]\\|pack\\)?")
.PHONY: all clean $(BINDIR)/$(TARGET)

all: $(BINDIR) $(BINDIR)/$(TARGET) $(SOURCES)

$(BINDIR):
	mkdir $(BINDIR)

$(BINDIR)/$(TARGET): $(SOURCES)
	ocamlbuild -lib str -Is src $(TARGET).native
	cp -L $(TARGET).native $@
	rm $(TARGET).native

clean:
	ocamlbuild -clean
	rm -f $(BINDIR)/$(TARGET)
