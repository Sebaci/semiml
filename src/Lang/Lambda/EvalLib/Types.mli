
module Env     : Map.S with type key = Common.Var.t
module ValHeap : module type of Common.Heap
module IntHeap : module type of Common.Heap

exception Type_error

type val_loc = ValHeap.loc
type int_loc = IntHeap.loc

type answer = unit

type value =
| V_Record    of value list
| V_Int       of int64
| V_Real      of Common.Real.t
| V_Func      of (value -> cont -> state -> answer)
| V_String    of string
| V_ByteArray of int_loc list
| V_Array     of val_loc list
| V_UArray    of int_loc list
| V_Cont      of cont
| V_ValRef    of val_loc
| V_IntRef    of int_loc
| V_Tagged    of value * int
| V_Exn       of value * val_loc

and cont    = value -> state -> answer
and handler = value -> state -> answer
and state   = 
  { st_val_heap : value ValHeap.t
  ; st_int_heap : int64 IntHeap.t
  ; st_handler  : handler
  }

module Value : sig
  val as_record  : value -> value list
  val as_int     : value -> int64
  val as_real    : value -> Common.Real.t
  val as_func    : value -> (value -> cont -> state -> answer)
  val as_cont    : value -> cont
  val as_val_ref : value -> val_loc
  val as_exn     : value -> value * val_loc

  val is_boxed : value -> bool
end
