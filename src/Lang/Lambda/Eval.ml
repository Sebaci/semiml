open Ast
open EvalLib.Types

(** [match_con value con] checks if [value] matches contructor [con] *)
let match_con value con =
  match con with
  | C_Tagged n ->
    begin match value with
    | V_Tagged(_, m) -> n = m
    | _ -> false
    end
  | C_Transparent -> true
  | C_TransU -> not (Value.is_boxed value)
  | C_TransB -> Value.is_boxed value

let in_constant_range range n =
  match range with
  | None -> true
  | Some r -> n >= 0L && n < r

let apply_con con value =
  match con with
  | C_Tagged n -> V_Tagged(value, n)
  | C_Transparent -> value
  | C_TransU ->
    if Value.is_boxed value then raise Type_error
    else value
  | C_TransB ->
    if Value.is_boxed value then value
    else raise Type_error

let apply_decon con value =
  match con, value with
  | C_Tagged n, V_Tagged(v, m) when n = m -> v
  | C_Tagged _, _ -> raise Type_error
  | C_Transparent, _ -> value
  | C_TransU, _ ->
    if Value.is_boxed value then raise Type_error
    else value
  | C_TransB, _ ->
    if Value.is_boxed value then value
    else raise Type_error

let rec eval e env cont state =
  match e.e_kind with
  | Var x -> 
    begin match Env.find x env with
    | value -> cont value state
    | exception Not_found ->
      assert false (* Broken contract *)
    end
  | Fn(x, body) ->
    let func v c s = eval body (Env.add x v env) c s in
    cont (V_Func func) state
  | Fix(defs, expr) ->
    let env' = build_fix defs env () in
    eval expr env' cont state 
  | App(e1, e2) ->
    eval e1 env (fun v1 state ->
      let f = Value.as_func v1 in
      eval e2 env (fun v2 state ->
        f v2 cont state
      ) state
    ) state
  | Int n -> cont (V_Int n) state
  | Real r -> cont (V_Real r) state
  | String s -> cont (V_String s) state
  | Switch switch -> eval_switch switch env cont state
  | Con(con, expr) ->
    eval expr env (fun v state ->
      cont (apply_con con v) state
    ) state
  | Decon(con, expr) ->
    eval expr env (fun v state ->
      cont (apply_decon con v) state
    ) state
  | SwitchExn(expr, cases, default_case) ->
    eval expr env (fun v state ->
      let (_, loc) = Value.as_exn v in
      eval_switch_exn
        ~cases:        cases
        ~default_case: default_case
        ~exn_loc:      loc
        env cont state
    ) state
  | ConExn(e1, e2) ->
    eval e1 env (fun v1 state ->
      let exn_loc = Value.as_val_ref v1 in
      eval e2 env (fun v2 state ->
        cont (V_Exn(v2, exn_loc)) state
      ) state
    ) state
  | ExnValue expr ->
    eval expr env (fun v state ->
      let (value, _) = Value.as_exn v in
      cont value state
    ) state
  | ExnName expr ->
    eval expr env (fun v state ->
      let (_, loc) = Value.as_exn v in
      cont (V_ValRef loc) state
    ) state
  | Record fields ->
    eval_fields fields env (fun values state ->
      cont (V_Record values) state
    ) state
  | Select(i, expr) ->
    eval expr env (fun v state ->
      let r = Value.as_record v in
      if i < 0 || i >= List.length r then raise Type_error
      else cont (List.nth r i) state
    ) state
  | Raise expr ->
    eval expr env (fun v state ->
      let handler = state.st_handler in
      handler v state
    ) state
  | Handle(e1, e2) ->
    let handler = state.st_handler in
    let handler' ex state =
      let state = { state with st_handler = handler } in
      eval e2 env (fun h state ->
        let f = Value.as_func h in
        f ex cont state
      ) state in
    let state = { state with st_handler = handler' } in
    eval e1 env (fun v state ->
      let state = { state with st_handler = handler } in
      cont v state
    ) state
  | Prim(Primop.Arith prim) -> 
    cont (EvalLib.Primop.eval_prim_arith prim) state
  | Prim(Primop.FArith prim) -> 
    cont (EvalLib.Primop.eval_prim_farith prim) state
  | Prim(Primop.Mem prim) -> 
    cont (EvalLib.Primop.eval_prim_mem prim) state
  | Prim(Primop.Repr prim) -> 
    cont (EvalLib.Primop.eval_prim_repr prim) state
  | Prim(Primop.Control prim) -> 
    cont (EvalLib.Primop.eval_prim_control prim) state

and build_fix defs env () =
  match defs with
  | [] -> env
  | (f, { e_kind = Fn(x, body) }) :: defs ->
    let rec func v c s = eval body (Env.add x v (env' ())) c s
    and env' () = build_fix defs (Env.add f (V_Func func) env) () in
    env' ()
  | (_, _) :: _ -> raise Type_error

and eval_switch switch env cont state =
  eval switch.sw_expr env (fun v state ->
    if List.exists (match_con v) switch.sw_possible_cons then
      eval_switch_con
        ~cases:        switch.sw_con_cases
        ~default_case: switch.sw_default_case
        ~value:        v
        env cont state
    else
      let value = Value.as_int v in
      if in_constant_range switch.sw_constant_range value then
        eval_switch_constant
          ~cases:        switch.sw_constant_cases
          ~default_case: switch.sw_default_case
          ~value:        value
          env cont state
      else raise Type_error
  ) state

and eval_switch_con ~cases ~default_case ~value env cont state =
  match cases with
  | [] ->
    begin match default_case with
    | None      -> raise Type_error
    | Some expr -> eval expr env cont state
    end
  | (con, expr) :: cases ->
    if match_con value con then
      eval expr env cont state
    else
      eval_switch_con
        ~cases:        cases
        ~default_case: default_case
        ~value:        value
        env cont state

and eval_switch_constant ~cases ~default_case ~value env cont state =
  match cases with
  | [] ->
    begin match default_case with
    | None      -> raise Type_error
    | Some expr -> eval expr env cont state
    end
  | (n, expr) :: _ when n = value -> eval expr env cont state
  | _ :: cases ->
    eval_switch_constant
      ~cases:        cases
      ~default_case: default_case
      ~value:        value
      env cont state

and eval_switch_exn ~cases ~default_case ~exn_loc env cont state =
  match cases with
  | [] -> eval default_case env cont state
  | (e1, expr) :: cases ->
    eval e1 env (fun v state ->
      let loc = Value.as_val_ref v in
      if ValHeap.equal_loc loc exn_loc then
        eval expr env cont state
      else
        eval_switch_exn
          ~cases:        cases
          ~default_case: default_case
          ~exn_loc:      exn_loc
          env cont state
    ) state

and eval_fields fields env cont state =
  match fields with
  | [] -> cont [] state
  | expr :: fields ->
    eval expr env (fun v state ->
      eval_fields fields env (fun vs state ->
        cont (v :: vs) state
      ) state
    ) state

let eval_top expr =
  ()
