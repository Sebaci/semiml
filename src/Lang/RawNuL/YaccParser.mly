%token <string> ID
%token <int> NAT
%token EOF
%token BR_OPN BR_CLS
%token ARROW BAR
%token KW_CASE KW_FUN KW_OF KW_SUCC KW_ZERO

%type <Ast.expr> main
%start main

%{

let mk_expr kind =
  let tag = Common.Tag.fresh () in
  MetaData.Position.set tag (Common.Position.parsing_current_pos ());
  { Ast.e_tag  = tag
  ; Ast.e_kind = kind
  }

let chang_pos expr =
  MetaData.Position.set expr.Ast.e_tag 
    (Common.Position.parsing_current_pos ());
  expr

let mk_ident name =
  let tag = Common.Tag.fresh () in
  MetaData.Position.set tag (Common.Position.parsing_current_pos ());
  { Ast.id_tag  = tag
  ; Ast.id_name = name
  }

%}

%%

main
: expr EOF { $1 }

expr
: KW_FUN id_list1 ARROW expr {
    mk_expr (Ast.Abs($2, $4))
  }
| KW_CASE expr KW_OF case_fun case_zero case_succ {
    mk_expr (Ast.Case($2, $4, $5, $6))
  }
| expr_app { $1 }
;

expr_app
: expr_app expr_atom { mk_expr (Ast.App($1, $2)) }
| expr_atom { $1 }
;

expr_atom
: ID      { mk_expr (Ast.Var $1) }
| NAT     { mk_expr (Ast.Num $1) }
| KW_ZERO { mk_expr (Ast.Num 0) }
| KW_SUCC { mk_expr Ast.Succ }
| BR_OPN expr BR_CLS { chang_pos $2 }
;

case_fun
: BAR KW_FUN ident ARROW expr { ($3, $5) }
;

case_zero
: BAR KW_ZERO ARROW expr { $4 }
;

case_succ
: BAR KW_SUCC ident ARROW expr { ($3, $5) }
;

id_list1
: ident rev_ident_list { $1 :: List.rev $2 }
;

rev_ident_list
: /* empty */ { [] }
| rev_ident_list ident { $2 :: $1 }
;

ident
: ID { mk_ident $1 }
;
