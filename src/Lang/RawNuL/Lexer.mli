
exception Invalid_character of char
exception Invalid_number    of string

val token : Lexing.lexbuf -> YaccParser.token
