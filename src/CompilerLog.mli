val debug   : ('a, out_channel, unit) format -> 'a
val info    : ('a, out_channel, unit) format -> 'a
val warning : ('a, out_channel, unit) format -> 'a
val error   : ('a, out_channel, unit) format -> 'a
