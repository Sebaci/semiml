
module type S = sig
  type key
  type t

  val create : 
    ?attributes: Box.attribute list -> 
      (key -> string) -> t

  val pretty : t -> key -> Box.t
  val extend : t -> key -> t * Box.t
end

module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module Make(Key : OrderedType) : S with type key = Key.t
