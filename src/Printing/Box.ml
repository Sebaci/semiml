
type attribute = DataTypes.attribute =
| Error
| Keyword
| Number
| Operator
| Paren
| Variable

type t = DataTypes.box =
| B_Text      of attribute list * string
| B_Prefix    of t * t
| B_Suffix    of t * t
| B_Indent    of int * t
| B_WhiteSep  of t
| B_BreakLine of t
| B_Box       of t list

let text ?(attributes=[]) str = B_Text(attributes, str)

let error   str = text ~attributes:[Error] str
let keyword str = text ~attributes:[Keyword] str
let oper    str = text ~attributes:[Operator] str

let prefix box1 box2 = B_Prefix(box1, box2)
let suffix box1 box2 = B_Suffix(box1, box2)

let indent n box = B_Indent(n, box)

let white_sep box = B_WhiteSep box

let br box = B_BreakLine box

let box boxes = B_Box boxes

let int n = text ~attributes:[Number] (string_of_int n)

let paren ?(opn="(") ?(cls=")") box =
  prefix (text ~attributes:[Paren] opn)
    (suffix box (text ~attributes:[Paren] cls))

let prec_paren ?(opn="(") ?(cls=")") box_prec expected_prec box =
  if expected_prec > box_prec then 
    paren ~opn:opn ~cls:cls box
  else box

let print_stdout = Engine.print_stdout
