
type attribute =
| Error
| Keyword
| Number
| Operator
| Paren
| Variable

type box =
| B_Text      of attribute list * string
| B_Prefix    of box * box
| B_Suffix    of box * box
| B_Indent    of int * box
| B_WhiteSep  of box
| B_BreakLine of box
| B_Box       of box list
