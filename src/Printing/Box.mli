
type attribute =
| Error
| Keyword
| Number
| Operator
| Paren
| Variable

type t

val text  : ?attributes: attribute list -> string -> t

val error   : string -> t
val keyword : string -> t
val oper    : string -> t

val indent    : int -> t -> t
val white_sep : t -> t
val br        : t -> t

val box : t list -> t

val int : int -> t

val paren : 
  ?opn: string ->
  ?cls: string ->
    t -> t

val prec_paren : 
  ?opn: string -> 
  ?cls: string ->
  int -> int -> t -> t

val print_stdout : t -> unit
