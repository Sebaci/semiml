
let _ =
  Components.init ()

let meta_commands = Queue.create ()

let comma_regexp = Str.regexp ","

let usage_string =
  "Usage: semiML [OPTION]... [FILE]...\nAvailable OPTIONs are:"

let cmd_args_options = Arg.align
  [ "-eval",   
    Arg.Unit (fun () -> Queue.add MetaCommand.MetaCmd_Eval meta_commands),
    " Evaluate a program"
  ; "-pretty", 
    Arg.Unit (fun () -> Queue.add MetaCommand.MetaCmd_Pretty meta_commands),
    " Pretty-print a program"
  ; "-require", 
    Arg.String (fun str ->
      let cmd = MetaCommand.MetaCmd_Require (Str.split comma_regexp str) in
      Queue.add cmd meta_commands),
    "CONTRACTS Compile to a state satisfying given CONTRACTS, "
    ^ "where CONTRACTS is a comma-separated list of contract names."
  ; "-check-contracts",
    Arg.Unit (fun () -> Settings.set_check_contracts true),
    " Dynamically checks all checkable contracts during the compilation."
  ; "-internal-log-file",
    Arg.String (fun filename ->
      match filename with
      | "stderr" -> Settings.set_internal_log_handle (Some stderr)
      | _        -> Settings.set_internal_log_handle (Some (open_out (filename)))
    ),
    "FILENAME|stderr Write internal compilation log to FILENAME or stderr"
  ]

let proc_fname fname =
  let state = CompilerGraph.parse_file fname in
  let _ : Compiler.state = 
    Queue.fold MetaCommand.run state meta_commands
  in ()

let _ =
  Arg.parse cmd_args_options proc_fname usage_string
