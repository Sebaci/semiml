
type loc
type 'value t

val empty : 'value t
val mk_ref : 'value t -> 'value -> 'value t * loc
val get    : 'value t -> loc -> 'value
val set    : 'value t -> loc -> 'value -> 'value t

val equal_loc : loc -> loc -> bool
