
type t = float

let is_boxed () = true

let add x y = x +. y
let sub x y = x -. y
let mul x y = x *. y
let div x y = x /. y

let equal     x y = x = y
let not_equal x y = x <> y

let lt x y = x < y
let le x y = x <= y
let gt x y = x > y
let ge x y = x >= y
