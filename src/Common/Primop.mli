
module Arith : sig
  type t =
  | Add | Sub
  | Neg
  | Mul | Div | Mod
  | RShift | LShift
  | Orb | Andb | Xorb
  | Notb
  | Eq  | NEq
  | Lt  | Le  | Gt  | Ge
  | RangeCheck
end

module FArith : sig
  type t =
  | FAdd | FSub
  | FMul | FDiv
  | FEq  | FNeq 
  | FLt  | FLe  | FGt  | FGe
end

module Mem : sig
  type t =
  | Deref | Subscript | OrdOf
  | Assign | UnboxedAssign
  | Update | UnboxedUpdate | Store
  | MakeRef | MakeRefUnboxed
end

module Repr : sig
  type t =
  | Boxed
  | ALength | SLength
end

module Exn : sig
  type t =
  | GetHdlr
  | SetHdlr
end

module Control : sig
  type t =
  | CallCC
  | Throw
end
