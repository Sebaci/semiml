module IntMap = Map.Make(struct type t = int let compare = compare end)

type loc = int
type 'value t =
  { next : loc
  ; heap : 'value IntMap.t
  }

let empty =
  { next = 0
  ; heap = IntMap.empty
  }

let mk_ref heap value =
  { next = heap.next + 1
  ; heap = IntMap.add heap.next value heap.heap
  }, heap.next

let get heap loc =
  IntMap.find loc heap.heap

let set heap loc value =
  { heap with
    heap = IntMap.add loc value heap.heap
  }

let equal_loc l1 l2 = l1 = l2
