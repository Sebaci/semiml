
type 'a language = 'a CompilerDB.Types.language =
| Lang_Lambda : Lang.Lambda.Ast.expr language
| Lang_NuL    : Lang.NuL.Ast.expr language
| Lang_RawNuL : Lang.RawNuL.Ast.expr language

type 'lang analysis =
  'lang -> unit

type 'lang contract_checker =
  'lang -> bool

type 'lang evaluator =
  'lang -> unit

type 'lang parser_func = 
  Lexing.lexbuf -> 'lang

type 'lang pretty_printer =
  'lang -> Printing.Box.t

type ('source, 'target) transformation =
  'source -> 'target

type state = CompilerDB.Types.state =
| State : 'lang language * 'lang * Contract.Set.t -> state

val register_analysis :
  lang:       'lang language ->
  name:       string ->
  ?require:   Contract.t list ->
  ?contracts: Contract.t list ->
  ?contract_rules: Contract.rule list ->
    'lang analysis -> unit

val register_contract_checker :
  lang:     'lang language ->
  contract: Contract.t ->
  name:     string ->
    'lang contract_checker -> unit

val register_evaluator :
  lang:     'lang language ->
  name:     string ->
  ?require: Contract.t list ->
    'lang evaluator -> unit

val register_parser :
  lang:       'lang language ->
  name:       string ->
  ?extension: string ->
  ?contracts: Contract.t list ->
    'lang parser_func -> unit

val register_pretty_printer :
  lang:       'lang language ->
  name:       string ->
  ?require:   Contract.t list ->
    'lang pretty_printer -> unit

val register_transformation :
  source:         'source language ->
  target:         'target language ->
  name:            string ->
  ?require:        Contract.t list ->
  ?contracts:      Contract.t list ->
  ?contract_rules: Contract.rule list ->
    ('source, 'target) transformation -> unit

