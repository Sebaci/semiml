
type 'lang pretty_data =
  { pretty  : 'lang Types.pretty_printer
  ; name    : string
  ; require : Contract.t list
  }

type t =
| Pretty : 'lang Types.language * 'lang pretty_data -> t
