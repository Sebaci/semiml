
type 'lang evaluator_data =
  { eval    : 'lang Types.evaluator
  ; name    : string
  ; require : Contract.t list
  }

type t =
| Evaluator : 'lang Types.language * 'lang evaluator_data -> t
