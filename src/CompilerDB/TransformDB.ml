
open Transform

let transformation_list = ref []

let register_transformation transform_obj =
  transformation_list := transform_obj :: !transformation_list

let possible_transforms lang contracts =
  List.filter (fun transform_obj ->
    match transform_obj with
    | Transform(lang2, _, data) ->
      Types.Language.equal lang lang2 &&
      List.for_all (fun c -> Contract.Set.mem c contracts) data.require
  ) !transformation_list
