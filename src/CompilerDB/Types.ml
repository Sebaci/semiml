
type _ language =
| Lang_Lambda : Lang.Lambda.Ast.expr language
| Lang_NuL    : Lang.NuL.Ast.expr language
| Lang_RawNuL : Lang.RawNuL.Ast.expr language

type 'lang analysis =
  'lang -> unit

type 'lang contract_checker =
  'lang -> bool

type 'lang evaluator =
  'lang -> unit

type 'lang parser_func = 
  Lexing.lexbuf -> 'lang

type 'lang pretty_printer =
  'lang -> Printing.Box.t

type ('source, 'target) transformation =
  'source -> 'target

type state =
| State : 'lang language * 'lang * Contract.Set.t -> state

module Language = struct
  type 'a t = 'a language

  let compare (type a) (type b) (l1 : a t) (l2 : b t) =
    match l1, l2 with
    | Lang_Lambda, Lang_Lambda -> 0
    | Lang_Lambda, _           -> -1
    | _,           Lang_Lambda -> 1
    | Lang_NuL,    Lang_NuL    -> 0
    | Lang_NuL,    _           -> -1
    | _,           Lang_NuL    -> 1
    | Lang_RawNuL, Lang_RawNuL -> 0

  let equal l1 l2 = compare l1 l2 = 0
  
  let get_equality (type a) (type b) (l1 : a t) (l2 : b t) 
      : (a, b) Equality.t =
    match l1, l2 with
    | Lang_Lambda, Lang_Lambda -> Equality.Eq
    | Lang_Lambda, _           -> Equality.Neq
    | _,           Lang_Lambda -> Equality.Neq
    | Lang_NuL,    Lang_NuL    -> Equality.Eq
    | Lang_NuL,    _           -> Equality.Neq
    | _,           Lang_NuL    -> Equality.Neq
    | Lang_RawNuL, Lang_RawNuL -> Equality.Eq
end
