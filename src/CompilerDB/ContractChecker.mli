
type 'lang contract_checker_data =
  { checker  : 'lang Types.contract_checker
  ; contract : Contract.t
  ; name     : string
  }

type t =
| ContractChecker : 'lang Types.language * 'lang contract_checker_data -> t
