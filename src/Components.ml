
open Compiler

let register_analyses () =
  Analysis.NuL.FreeVars.register ();
  ()

let register_transformations () =
  Transform.NuL.PartialEval.register ();
  Transform.NuL.ScopeCheck.register ();
  Transform.NuL.UniqueTags.register ();
  Transform.NuL.UniqueVars.register ();
  Transform.NuL.UnusedLet.register ();
  ()

let register_contract_checkers () =
  ContractCheck.NuL.RightScopes.register ();
  ContractCheck.NuL.UniqueTags.register ();
  ContractCheck.NuL.UniqueVars.register ();
  ()
 
let register_evaluators () =
  register_evaluator
    ~lang: Lang_NuL
    ~name: "NuL:eval"
    ~require: [ Lang.NuL.Contracts.right_scopes ]
    Lang.NuL.Eval.eval_program;
  ()

let register_parsers () =
  register_parser
    ~lang: Lang_RawNuL
    ~name: "RawNuL:parser"
    ~extension: "nul"
    Lang.RawNuL.Parser.parser_func;
  ()

let register_printers () =
  register_pretty_printer
    ~lang: Lang_NuL
    ~name: "NuL:pretty"
    Lang.NuL.Pretty.pretty_program;
  ()

let init () =
  register_analyses ();
  register_contract_checkers ();
  register_evaluators ();
  register_parsers ();
  register_printers ();
  register_transformations ();
  ()
