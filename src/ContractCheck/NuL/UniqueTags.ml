
open Lang.NuL.Ast

let used_tags = Hashtbl.create 32

let rec check_expr expr =
  if Hashtbl.mem used_tags expr.e_tag then false
  else begin
    Hashtbl.add used_tags expr.e_tag ();
    match expr.e_kind with
    | Succ | Var _ | Num _ -> true
    | Abs(_, body) -> check_expr body
    | App(e1, e2)  -> check_expr e1 && check_expr e2
    | Case(e1, (_, e2), e3, (_, e4)) ->
      check_expr e1 && check_expr e2 && check_expr e3 && check_expr e4
  end

let check_program expr =
  Hashtbl.reset used_tags;
  check_expr expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_NuL
    ~name: "NuL:unique_tags"
    ~contract: Lang.NuL.Contracts.unique_tags
    check_program
