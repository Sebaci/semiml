open Lang.NuL.Ast

(* I assume I am running AFTER ScopeChecker *)
let rec rename x y { e_tag; e_kind } =
  let rename_kind kind = match kind with
    | Succ | Num _ -> kind
    | Var z        -> if Common.Var.equal z x then Var y else Var z
    | Abs (z, e)   -> Abs (z, rename x y e)
    | App (e1, e2) -> App (rename x y e1, rename x y e2)
    | Case (e, (v1, e1), e2, (v3, e3)) ->
        Case (rename x y e, (v1, rename x y e2), rename x y e2, (v3, rename x y e3))
  in
  { e_tag; e_kind = rename_kind e_kind }

let rec reduce { e_tag; e_kind } =
  let reduce_kind kind = match kind with
    | App ({e_tag = _; e_kind = Abs (var, body)}, {e_tag = _; e_kind = Var var'}) ->
      (reduce (rename var var' body)).e_kind
    | App (e1, e2)    -> App (reduce e1, reduce e2)
    (* TODO: "partial_eval" case patterns *)
    | Case (e, (v1, e1), e2, (v3, e3)) ->
        Case (reduce e, (v1, reduce e1), reduce e2, (v3, reduce e3))
    | Abs (var, body) -> Abs (var, reduce body)
    | x -> x
  in
  { e_tag; e_kind = reduce_kind e_kind }

let c_partial_eval = Contract.create "transform:partial_eval"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_NuL
    ~target: Compiler.Lang_NuL
    ~name:   "NuL:partial_eval"
    ~require:
      [ Lang.NuL.Contracts.right_scopes
      ; Lang.NuL.Contracts.unique_vars
      ]
    ~contracts:
      [ Lang.NuL.Contracts.right_scopes
      ; Lang.NuL.Contracts.unique_vars
      ; c_partial_eval
      ]
    reduce
